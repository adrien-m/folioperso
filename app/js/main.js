    $(document).ready(function(){

        // if (typeof $dom !== 'undefined') {
        //   // Désormais, on sait que toto est bien
        //   // défini et on peut poursuivre.
        // }

        // loading();
        hoverImg();

        showMain();

        gallery1 = new wtfGallery($('.gallery.one'));
        gallery2 = new wtfGallery($('.gallery.two'));
        gallery3 = new wtfGallery($('.gallery.three'));
        gallery4 = new wtfGallery($('.gallery.four'));
        gallery5 = new wtfGallery($('.gallery.five'));
        gallery6 = new wtfGallery($('.gallery.six'));

        // function loading() {

        // setTimeout( function(){
            // TweenMax.to($('.loader'), 1, {opacity: 0,ease:Quad.easeOut});
            TweenMax.to($('.main'), 2.2, {opacity: 1, scale: "1",ease:Quad.easeOut});
            TweenMax.to($('.nav'), 1.2, {opacity: 1,ease:Quad.easeOut});
            // $(".loader").css("display", "none").delay(3000);
            // $(".loader").hide().delay(3000);

            $(".nav").addClass("show");
        // }, 3500);

    // }

    function hoverImg() {

        $(".span").mouseenter( function(){

            var dataArt = $(this).attr("data-art");
            
            $(".img-bg").attr("src", "../img/" + dataArt + ".png");

            TweenMax.to($('.img-art'), 0.8, {opacity: 1, left: "35%",ease:Quad.easeOut}); //TWEENMAX HERE//
            TweenMax.to($('.img-dev'), 0.8, {opacity: 1, right: "35%",ease:Quad.easeOut});
            TweenMax.to($('.img-info'), 0.8, {opacity: 1, bottom: "35%",ease:Quad.easeOut});
        });

        $(".span").mouseout( function(){
            TweenMax.to($('.img-art'), 0.6, {opacity: 0, left: "30%",ease:Quad.easeOut});
            TweenMax.to($('.img-dev'), 0.6, {opacity: 0, right: "30%",ease:Quad.easeOut});
            TweenMax.to($('.img-info'), 0.6, {opacity: 0, bottom: "30%",ease:Quad.easeOut});
        });

    };

    function showMain() {

        $(".main-js").bind("click", function(e) {



            e.preventDefault();

            $("section").addClass("hide");
            // $(".main").removeClass("hide");
            TweenMax.fromTo($(".main"), 1, {y:-100, opacity: 0, onComplete: function() { $(".main").removeClass('hide'); }}, {y:0, opacity: 1});
            // $(".main").show();

        })

    }

    var activeTarget;
    var animProjet;
    var target;

// ANIMATION ON CLICK
    $('.anim-js').bind("click", function(e) {

        e.preventDefault();
        var main = $(this).attr("main");
        target = $(this).attr("data-target");
        var anim = $(this).attr("data-anim");
        animProjet = $(this).attr("data-animProjet");
        activeTarget = target;

        pageTransition($(target), anim);

    })
// FIN ANIMATION ON CLICK


    $(".link-projet-js").bind("click", function(e) {
        e.preventDefault();

        target = $(this).attr("data-target");

        pageTransition($(target), animProjet);

        // $(".slider").slick();

    })


    $(".retour-js").bind("click", function(e) {
        e.preventDefault();

        pageTransition($(activeTarget), animProjet);

    })

// ARROWS NAVIGATION
    $(window).keydown(function(e){

        if (e.keyCode == 37) { 
            pageTransition($('.art'), "left");
            return false;
        }
        if (e.keyCode == 38) {
            pageTransition($('.main'), "up");
            return false;
        }
        if (e.keyCode == 39) {
            pageTransition($('.dev'), "right");
            return false;
        }
        if (e.keyCode == 40) { 
            pageTransition($('.info'), "down");
            return false;
        }
    });
// END ARROWS NAVIGATION


// PAGE TRANSITION FUNCTION
    function pageTransition(target, anim) {

        if(target.hasClass("hide")){

            $('section').addClass('hide');

            switch(anim) {
                case "left" :
                    TweenMax.fromTo(target, 1, {x:-100, opacity: 0, onComplete: function() { target.removeClass('hide'); }}, {x:0, opacity: 1});
                break;

                case "right" :
                    TweenMax.fromTo(target, 1, {x:100, opacity: 0, onComplete: function() { target.removeClass('hide'); }}, {x:0, opacity: 1}); 
                break;

                case "down" :
                    TweenMax.fromTo(target, 1, {y:100, opacity: 0, onComplete: function() { target.removeClass('hide'); }}, {y:0, opacity: 1});
                break;
                case "top" :
                    TweenMax.fromTo(target, 1, {y:-100, opacity: 0, onComplete: function() { target.removeClass('hide'); }}, {y:0, opacity: 1});
                break;
            }

        } else{

            $('.main').removeClass('hide');

            switch(anim) {
                case "left" :
                    TweenMax.fromTo(target, 1, {x:0, opacity: 1}, {x:-100, opacity: 0, onComplete: function() { target.addClass('hide'); }});
                    // TWEEN MAIN //
                break;

                case "right" :
                    TweenMax.fromTo(target, 1, {x:0, opacity: 1}, {x:100, opacity: 0, onComplete: function() { target.addClass('hide'); }});
                    // TWEEN MAIN //
                break;

                case "down" :
                    TweenMax.fromTo(target, 1, {y:0, opacity: 1}, {y:100, opacity: 0, onComplete: function() { target.addClass('hide'); }});
                    // TWEEN MAIN //
                break;
                // case "top" :
                //     TweenMax.fromTo(target, 1, {y:0, opacity: 1}, {y:-100, opacity: 0, onComplete: function() { target.addClass('hide'); }});
                // break;
            }
        }
    }
// FIN PAGE TRANSITION FUNCTION

    });

    

    

    