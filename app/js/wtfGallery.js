var wtfGallery = function( $dom ){
  this.$dom = $dom;
  this.currentIndex;
  this.incrementation = -1;
  this.currentIndex = this.$dom.find('.slide').length - 1;
  
  this.randomPictsPos();
  this.bindEvents();
  
  this.doIntro();
}

wtfGallery.prototype.doIntro = function(){
  var self = this;
  this.$dom.find('.slide').each(function(index){
    TweenMax.from(self.$dom.find('.slide').eq(index), 0.5 + Math.random() * 0.5, { x: (Math.random()*self.$dom.width()*3)-self.$dom.width(), y: (Math.random()*parseFloat(self.$dom.height())*3)-window.innerHeight, ease: Back.easeOut, delay: 0.25*index });  
    TweenMax.to(self.$dom.find('.slide').eq(index), 0.25 + Math.random() * 0.5, { opacity: 1, ease: Back.easeOut, delay: 0.25*index });      
  });
}

wtfGallery.prototype.bindEvents = function(){  
  var self = this;
  $(window).bind('resize', function(){
    self.setPictsPos();
  });
    
  this.$dom.bind('click', function(e){
    self.$dom.find('.slide').eq(self.currentIndex).toggle();
    
    self.currentIndex += self.incrementation;
    
    if(self.currentIndex == 0){
       self.currentIndex = 1;
      self.incrementation = -self.incrementation;
    }   
    
    if(self.currentIndex > self.$dom.find('.slide').length - 1){
       self.currentIndex = self.$dom.find('.slide').length - 1;
       self.incrementation = -self.incrementation;
    }
    
    e.preventDefault();
  });
}
 
wtfGallery.prototype.randomPictsPos = function(){
  var self = this;
  this.$dom.find('.slide').each(function(index){
    var $currentPict = self.$dom.find('.slide').eq(index); 
    var xPos = (Math.random()*0.2)+0.15;
    var yPos = Math.random()*0.2;
    
    // TweenMax.set( $currentPict, { scale: (Math.random()*0.4)+0.55 } );
    TweenMax.set( $currentPict, { left: xPos * (self.$dom.width() - parseInt($currentPict.width()) ) + 'px', top: yPos * (self.$dom.height() - parseInt($currentPict.height())) + 'px' } ); 
    
    $currentPict.attr({ 'data-xpos': xPos, 'data-ypos': yPos })


    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        var xPos = (Math.random()*0.1)+5;
        var yPos = Math.random()*0.15;
    }
    
  });
}
 
wtfGallery.prototype.setPictsPos = function(){
  var self = this;
  this.$dom.find('.slide').each(function(index){
    var $currentPict = $('.slide').eq(index);   
    
    TweenMax.set( $currentPict, { left: parseFloat($currentPict.attr('data-xpos')) * (self.$dom.width() - parseInt($currentPict.width())) + 'px', top:  parseFloat($currentPict.attr('data-ypos')) * (self.$dom.height() - parseInt($currentPict.height())) + 'px' } ); 
    
    console.log(parseFloat($currentPict.height()))
    
  });
}